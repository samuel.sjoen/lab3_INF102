package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        numbers.sort(null);
        int lastIndex = numbers.size()-1;
        if (numbers.size() == 1) {
            return numbers.get(lastIndex);
        }
        if (numbers.get(lastIndex) < numbers.get(lastIndex-1)) {
            numbers.remove(lastIndex);
            return peakElement(numbers);
        }
        else {
            return numbers.get(lastIndex);
        }
    }

}
