package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    private int lowerbound;
    private int upperbound;

    @Override
    public int findNumber(RandomNumber number) {
        if (lowerbound == 0 && upperbound == 0) {
            lowerbound = number.getLowerbound();
            upperbound = number.getUpperbound();
        }
        int size = upperbound - lowerbound;
        int middle = lowerbound + size / 2;
        int result = number.guess(middle);
        if (result == 1) {
            upperbound = middle;
            return findNumber(number);
        }
        if (result == -1) {
            lowerbound = middle;
            return findNumber(number);
        }
        else {
            lowerbound = 0;
            upperbound = 0;
            return middle;
        }
    }
}
